import dotenv from 'dotenv';
import axios from 'axios';
import { URL, URLSearchParams } from 'url';
import { parseStringPromise } from 'xml2js';

dotenv.config();

const apiBase = 'https://secure.networkmerchants.com/api';
const cartURLBase = 'https://secure.nmi.com/collect-checkout';

export function fetchCartURL({ sku, desc, quantity = 1 } = {}) {
  const {
    CHECKOUT_KEY,
    REDIRECT_BASE_URL
  } = process.env;

  const product = { sku, quantity };
  if (desc) product.description = desc;

  const options = {
    key: CHECKOUT_KEY,
    type: 'sale',
    lineItems: [product],
    receipt: {
      showReceipt: false,
      redirectToSuccessUrl: false,
      sendToCustomer: true
    },
    customerVault: {
      addCustomer: true
    },
    paymentMethods: [{
      type: 'creditCard'
    }]
  };

  if (REDIRECT_BASE_URL) {
    options.successUrl = `${REDIRECT_BASE_URL}/success/{TRANSACTION_ID}`;
    options.cancelUrl = `${REDIRECT_BASE_URL}/cancel`;
    options.receipt.redirectToSuccessUrl = true;
  }

  return axios
    .post(`${apiBase}/v4/cart`, options)
    .then(({ data }) => {
      if ('id' in data) {
        return `${cartURLBase}/?cartId=${data.id}`;
      } else {
        throw new Error('Missing Cart ID');
      }
    });
}

const parseXML = (xml, opts = {}) => parseStringPromise(xml, {
  explicitArray: false, ignoreAttrs: true,
  valueProcessors: [(val, key) => {
    if (typeof val !== 'string')
      return val;
    else if (val.trim().length < 1)
      return null;
    else if (/^(true|y(es)?)$/i.test(val))
      return true;
    else if (/^(false|no?)$/i.test(val))
      return false;
    else if (/success/i.test(key) && val.length < 2)
      return val === '1';
    else if (/date|time|\bat$/i.test(key) && /^20\d{12}$/.test(val))
      return parseDate(val);
    else if (!isNaN(parseFloat(val)) && isFinite(val))
      return Number(val);
    return val;
  }], ...opts
});

const parseDate = dateStr => new Date(dateStr
  .replace(/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/,
  '$1-$2-$3T$4:$5:$6'
));

export function query(params = {}) {
  const searchParams = new URLSearchParams({
    security_key: process.env.SECURITY_KEY, ...params
  });
  const url = new URL(`${apiBase}/query.php`);
  url.search = searchParams.toString();
  return axios.get(url.toString())
    .then(res => parseXML(res.data))
    .then(xml => xml.nm_response);
}

export function fetchTransaction(transactionId) {
  return query({transaction_id: transactionId})
    .then(xml => xml.transaction);
}

export function fetchOrder(orderId) {
  return query({order_id: orderId})
    .then(xml => xml.order);
}

export function latestTransactionDate(transactionId) {
  return fetchTransaction(transactionId)
    .then(transaction => transaction.action
    .map(action => parseDate(action.date))
    .sort().pop());
}
