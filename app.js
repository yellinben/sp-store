import createError from 'http-errors';
import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import dotenv from 'dotenv';

import { fetchCartURL, fetchTransaction } from './services/nmi.js';

const __dirname = path.dirname(new URL(import.meta.url).pathname);
dotenv.config();

const app = express();
const port = process.env.PORT || 5000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', async (req, res, next) => {
  res.redirect('https://thespaceperspective.com');
});

app.post('/checkout', async (req, res, next) => {
  const { sku, quantity, desc } = req.body;

  if (sku) {
    const cartURL = await fetchCartURL({ sku, quantity, desc });
    res.redirect(cartURL);
  } else {
    return next(new Error('Product SKU Required'));
  }
});

app.get('/checkout/:sku', async (req, res, next) => {
  const sku = req.params.sku;
  const cartURL = await fetchCartURL({ sku });
  if (cartURL) {
    res.redirect(cartURL);
  } else {
    return next(new Error('Product SKU Required'));
  }
});

app.get('/success/:transaction', async (req, res, next) => {
  const transaction = await fetchTransaction(req.params.transaction);
  if (transaction) {
    res.locals.transaction = transaction;
    res.render('success');
  }
});

app.get('/cancel', (req, res, next) => {
  res.render('cancelled');
});

app.use((req, res, next) => {
  next(createError(404));
});

app.use((err, req, res, next) => {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
  res.render('error');
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});

export default app;
